# na17p2

**Projet 2 NA17**

**Sujet**: 7. Dashboard

**Nom**: Jinane Ben Salem

**Scripts**
- init.sh: création user et database
- create.sql: script de création des tables
- insert.sql: script d'insertion de données
- select.sql: script de création des vues
- script.py: programme menu interactif
  - identifiants (table utilisateur):
    1. lhauchard@lilo.org / lucashauchard
    2. alain.corre@lilo.org / alaincorre_12345
    3. yves-guillemot@lilo.org / yvesguillemot
    4. etc.

https://librecours.net/dawa/projets-2020-II/co/dashboard.html

**Travail à rendre**
1. Étape 1 à rendre 2 jours avant votre TD2 : NDC, MCD
2. Étape 2 à rendre 2 jours avant votre TD3 : NDC, MCD, MLD, SQL CREATE et INSERT
3. Étape 3 à rendre 2 jours avant votre TD4 : NDC, MCD, MLD, SQL CREATE et INSERT, SQL SELECT, Complément : Normalisation (NF18, AI23) ou PostgreSQL/JSON (NA17, NF17)
4. Étape 4 à rendre 2 jours avant votre TD5 : Révision complète du projet (v2)


**Description générale**

Nous sommes une startup récemment intégré dans le prestigieux incubateur X-Combinator.

Nous développons des solutions innovantes de tableaux de bord (dashboard) collaboratifs à destination des entreprises.
Objectif du projet

Réaliser un client web (frontend utilisateur), le backend et la structure de données associés permettant de montrer à nos investisseurs le fonctionnement de notre idée révolutionnaire. Une attention particulière sera apportée à construire un modèle de données robuste. En effet nos meilleurs experts marketing prévoient plusieurs millions d'utilisateurs dès la semaine de lancement.
Liste non exhaustive des besoins identifiés par les architectes systèmes

L'application doit permettre de gérer les entités suivantes :

    Utilisateur: Email, Nom, Prénom, Age, Date de naissance, Entreprise, Genre, Pays, Métier...

    Flux De Publication: Titre_Flux, Responsable, Confidentialité...

    Publication: Titre, Lien, Date de publication, Créateur...

    Groupe d'Utilisateur: Nom, Responsable...

    Des suggestions ?

**Hypothèses complémentaires**

En plus de ces entités, les contraintes suivantes doivent être respectées :

    Chaque flux possède au minimum un Groupe Utilisateur « Rédacteur », et un « Lecteur ». Bien sûr les « Rédacteurs » peuvent également lire.

    Un utilisateur étant dans la catégorie « Rédacteur » et « Lecteur » se verra attribué le plus fort privilège : « Rédacteur ».

    Règles de publication :

        Chaque Rédacteur peut modifier / supprimer ses propres publications.

        Un article n'est visible qu'une fois validé. Dans l'état « validé », il ne peut être modifié.

        Seul le responsable de flux peut valider / dévalider / rejeter / supprimer / modifier tous les articles de son flux.

        Après toute modification par le responsable d'une publication, celui-ci devient le rédacteur de la publication de la publication.

    Système de Vote :

        Chaque utilisateur d'un groupe peut « Like » ou « Dislike » une publication.

        Un score est attribué : +1 pour un like, -1 pour un dislike.

        Si une publication a un score de -X, elle devient automatiquement « rejetée » (X étant un paramètre contrôlable par l'administrateur de l'application).

        On ne peut bien sûr voter qu'une fois par Publication.

    Les flux de publications sont par défaut triés du plus récent au plus ancien. Cependant une option doit permettre de les trier du score le plus élevé au plus bas.

    Chaque utilisateur d'un flux peut poster des commentaires concernant une publication. Ces derniers apparaissent du plus anciens au plus récent.

    Tout utilisateur peut créer son flux de publication :

        Il en est automatique responsable.

        Il peut transférer la responsabilité à un autre utilisateur.

        Il peut définir quels sont les groupes « Redacteurs » et « Lecteurs ».

    Tout utilisateur peut créer des groupes, il en est automatiquement le responsable. Il peut effectuer les actions suivantes :

        Transférer sa responsabilité.

        Ajouter / Supprimer des utilisateurs.

    Chaque utilisateur possède une page "home" ou "profile". Cette dernière affiche plusieurs informations :

        Nom de l'utilisateur;

        Prénom de l'utilisateur;

        Email de l'utilisateur;

        Groupes auxquels appartient l'utilisateur;

        Un dashboard lui présentant tous les flux qu'il peut lire ;

    Une page d'administration de flux sera également à créer permettant de rajouter une publication à un flux. Accessible uniquement par les rédacteurs.

**Remarques**

    Vous avez toute latitude pour mener à bien ce projet. La description précédemment fournie n'est qu'un exemple que nos ingénieurs ont pu mettre en évidence. Nous sommes cependant convaincus que les experts que vous êtes pourront, sans problème, affiner le besoin et l'enrichir et c'est en ce sens que nous avons recruté. L'avenir de ce projet repose sur vous. Ne pas hésiter à modifier la description si nécessaire.

    On réalisera en priorité la base de données et le backend, y compris les vues servant pour le frontend ; le frontend est optionnel dans le cadre du projet, il ne sera développé que s'il reste du temps.
