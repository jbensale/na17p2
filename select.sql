DROP VIEW v_profil;
DROP VIEW v_author;
DROP VIEW v_dashboard_flux;
DROP VIEW v_publication_comments;
DROP VIEW v_publication_reponse;
DROP VIEW v_publication;
DROP VIEW v_experiences_ut;
DROP VIEW v_entreprise;
DROP VIEW v_groupes_ut;

-- view all groups of each user
CREATE VIEW v_groupes_ut AS
SELECT u.id as uid, u.nom, u.prenom, g.id as gid, g.nom as groupe
FROM utilisateur u JOIN groupe_utilisateurs gu ON (u.id = gu.id_ut) JOIN groupe g ON (gu.id_gr = g.id);

-- view companies with json format
CREATE VIEW v_entreprise AS
SELECT id, nom, description, nationalite, president, taille, siege->>'adresse' as adresse, siege->>'ville' as ville, siege->>'cp' as cp, siege->>'pays' as pays
FROM entreprise;

-- view all experiences of user
CREATE VIEW v_experiences_ut AS
SELECT u.id, u.nom, u.prenom, xp->>'metier' as metier, xp->>'duree' as duree, xp->>'entreprise' as entreprise
FROM utilisateur u, JSON_ARRAY_ELEMENTS(u.experiences) xp;

-- view publication with score and number of comments
CREATE VIEW v_publication AS
SELECT p.id as pid, p.titre as titre_publication, p.auteur as auteur_id, CONCAT(u.prenom, ' ', u.nom) as auteur, p.creation, p.contenu, f.id as fid, f.titre as titre_flux, f.activite, p.likes + p.dislikes as score, COUNT(com.id) as nombre_commentaires
FROM publication p JOIN flux f ON (p.flux = f.id) JOIN commentaire com ON (com.publication = p.id AND com.flux = p.flux) JOIN utilisateur u ON (p.auteur = u.id)
GROUP BY p.id, p.titre, p.auteur, p.contenu, p.creation, f.id, f.titre, f.activite, p.likes + p.dislikes, u.prenom, u.nom;

-- view publication referenced by others
CREATE VIEW v_publication_reponse AS
SELECT p.id as pid, p.titre as publication, r.titre as reference
FROM publication p JOIN publication r ON (p.reference = p.id);

-- view publications comments
CREATE VIEW v_publication_comments AS
SELECT com.texte as commentaire, p.id as pid, p.titre as publication, com.creation, com.likes, com.dislikes
FROM publication p JOIN commentaire com ON (p.id = com.publication AND p.flux = com.flux);

-- view user as reader and writer of feeds
CREATE VIEW v_dashboard_flux AS
SELECT g.uid, CONCAT(g.prenom, ' ', g.nom) as utilisateur, 'lecteur' as droits, fl.titre as flux
FROM v_groupes_ut g JOIN lecteur_flux l ON (g.gid = l.id_gr) JOIN flux fl ON (l.id_fl = fl.id)
UNION
SELECT g.uid as uid, CONCAT(g.prenom, ' ', g.nom) as utilisateur, 'rédacteur' as droits, fr.titre as flux
FROM v_groupes_ut g JOIN redacteur_flux r ON (g.gid = r.id_gr) JOIN flux fr ON (r.id_fl = fr.id);

-- view comments and publications written by user
CREATE VIEW v_author AS
SELECT u.id as uid, CONCAT(u.prenom, ' ', u.nom) as auteur, 'a commenté' as type, com.creation as date, p.titre_publication as publication, p.titre_flux as flux, com.texte as contenu
FROM utilisateur u JOIN commentaire com ON (u.id = com.auteur) JOIN v_publication p ON(com.publication = p.pid)
UNION
SELECT u.id as uid, CONCAT(u.prenom, ' ', u.nom) as auteur, 'a rédigé' as type, p.creation as date, p.titre_publication as publication, p.titre_flux as flux, p.contenu as contenu
FROM utilisateur u JOIN v_publication p ON (u.id = p.auteur_id);

-- view profil of users with total of publications, groups and feeds
CREATE VIEW v_profil AS
SELECT u.id as uid, CONCAT(u.prenom, ' ', u.nom) as utilisateur, u.email, u.genre, u.anniversaire, u.pays, e.nom as entreprise,
  (
    SELECT COUNT(*) as groupes_membre
    FROM v_groupes_ut g
    WHERE u.id = g.uid
  )
FROM
  utilisateur u
  JOIN entreprise e ON(u.entreprise = e.id)
GROUP BY u.id, u.prenom, u.nom, u.email, u.pays, e.nom, u.genre, u.anniversaire;
