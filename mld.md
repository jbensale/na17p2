Utilisateur(#id: Integer, email: String, nom:String, prenom: String, pays: String, naissance: Date, experiences: , genre: {'f'|'m'|'o'}, entreprise=>Entreprise)
  avec {email unique}, {email, nom, prenom, entreprise NOT NULL}

Entreprise(#id: Integer, nom: String, nationalite: String, siege: String, description: String, president: String, taille: INTEGER)
  avec {nom, description NOT NULL}, {taille > 0}

Experience(metier: String, duree: String, utilisateur=>Utilisateur)

Groupe(#id: Integer, nom: String, confidentialite: {'publ'|'priv'|'prot'}, responsable=>Utilisateur)
  avec {nom, confidentialite, responsable NOT NULL}

GroupeUtilisateur(#utilisateur=>Utilisateur, #groupe=>Groupe)

Flux(#id: Integer, titre: String, description: String, activite: {'actif'|'inactif'|'archive'}, theme=>Theme, responsable=>Utilisateur)
  avec {titre, theme, responsable NOT NULL}, {activité = 'actif' par défaut}

Redacteur(#flux=>Flux, #groupe=>Utilisateur)
Lecteur(#flux=>Flux, #groupe=>Utilisateur)

Publication(#id: Integer, titre: String, contenu: Text, creation: Date, #flux=>Flux, etat: {'brouillon'|'validé}, auteur=>Utilisateur)
  avec {titre, contenu, creation, auteur NOT NULL}, {id as local key}

Commentaire(#id: Integer, texte: Text, creation: Date, likes: Integer, dislikes: Integer, #publication=>Publication, auteur=>Utilisateur)
  avec {texte, creation, publication NOT NULL}, {id as local key}

Theme(#nom: String, description: String)
  avec {description NOT NULL}
