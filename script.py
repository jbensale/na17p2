#!/usr/bin/python3

# credentials: lhauchard@lilo.org  lucashauchard
#              alain.corre@lilo.org alaincorre_12345

import psycopg2
import re

CO = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % ('localhost', 'postgres', 'jinane', '0000'))
USER = {
    'id': -1,
    'email': '',
    'password': '',
    'nom': '',
    'prenom': ''
}

def welcome():
    print('Bonjour\nIdentifiez-vous pour accéder à votre profil:\n')
    email = input('Adresse mail > ')
    # while EMAIL not matching
    while not re.search(r".+@.+\..+", email):
        email = input('Erreur\nEntrez une adresse mail correcte > ')

    password = input('Mot de passe > ')

    return {"email": email, "password": password}

def identify(email, password):
    sql_identify = "SELECT * FROM utilisateur WHERE email='%s' AND motdepasse='%s';" % (email, password)

    cur = CO.cursor()

    try:
        cur.execute(sql_identify)
        row = cur.fetchone()

        if row is None:
            return False
        if row[1] == email and row[2] == password:
            USER['id'] = row[0]
            USER['nom'] = row[3]
            USER['prenom'] = row[4]
            print('\nBienvenue', row[4])
            return True
    except psycopg2.Error as e:
        print(e)
    return False

def create_group():
    groupname = input('Nom du groupe > ')
    rights = input('Confidentialité du groupe (protected, private, public) > ')
    while not rights in ['protected', 'private', 'public']:
        rights = input('[protected, private, public] > ')

    print(rights[0:4])
    sql_insert = "INSERT INTO groupe(nom, confidentialite, responsable) VALUES('%s', '%s', %s);" % (groupname, rights[0:4], USER['id'])
    cur = CO.cursor()

    try:
        cur.execute(sql_insert)
        CO.commit()
        print('Vous être devenu le responsable du groupe %s' % groupname)
    except psycopg2.Error as e:
        print('Une erreur est survenu', e)

    return

def search_user():
    results = []
    target = input('Nom/prénom/email recherché > ')

    sql_search = "SELECT * FROM utilisateur WHERE LOWER(email) LIKE '%{0}%' OR LOWER(nom) LIKE '%{0}%' OR LOWER(prenom) LIKE '%{0}%';".format(target)

    cur = CO.cursor()

    try:
        cur.execute(sql_search)
        row = cur.fetchone()
        print('==> Voici les résultats:')
        while row:
            print('[{0}] {1} {2}'.format(row[0], row[3], row[4]))
            results.append(row[0])
            row = cur.fetchone()

        id = input('\n==> Saisir l\'identifiant d\'un utilisateur pour voir son profil\nId > ')

        if id.isnumeric() and int(id) in results:
            display_profil(id)
        else:
            print('Retour au menu ===')
    except psycopg2.Error as e:
        print(e)

    return

def display_dashboard(id):
    sql_author = "SELECT auteur, type, publication, flux, contenu, date FROM v_author a WHERE uid='%s';" % (id)
    cur = CO.cursor()

    try:
        cur.execute(sql_author)
        row = cur.fetchone()
        if row is None:
            print('Aucune activité')
            return

        print('=== Activité de {0} ==='.format(row[0]))
        while row:
            print('Le {4}: {0} {1} - flux {2}\n"{3}""\n'.format(row[1], row[2], row[3], row[4], row[5]))
            row = cur.fetchone()
        input('Continuer > ')

    except psycopg2.Error as e:
        print(e)

    return

def display_profil(id):
    sql_profil = "SELECT * FROM v_profil WHERE uid='%s';" % (id)

    cur = CO.cursor()

    try:
        cur.execute(sql_profil)
        row = cur.fetchone()

        if row is None:
            print('Aucun résultat')
            return

        print('\n=== Profil de {0} ==='.format(row[1]))
        print('Localisé à:', row[5] or 'inconnu')
        print('Travaille à: ', row[6] or 'inconnu')
        print('Contact:', row[2] or 'inconnu')
        print('Anniversaire:', row[4] or 'inconnu')

        if input('\nVoir ses activités? (o/n) > ') == 'o':
            display_dashboard(id)
            display_feeds(id)

    except psycopg2.Error as e:
        print(e)

    return

def display_feeds(id):
    sql_feeds_redacteur = "SELECT utilisateur, droits, flux FROM v_dashboard_flux d WHERE uid='%s' AND droits='rédacteur';" % (id)
    cur = CO.cursor()

    # Rédactions
    try:
        cur.execute(sql_feeds_redacteur)
        row = cur.fetchone()
        if row is None:
            print('Aucun flux de rédaction')
            return

        print('\n=== Rédactions de {0} ==='.format(row[0]))
        while row:
            print('{0}'.format(row[2]))
            row = cur.fetchone()

    except psycopg2.Error as e:
        print(e)

    # Lectures
    sql_feeds_lecteur = "SELECT utilisateur, droits, flux FROM v_dashboard_flux d WHERE uid='%s' AND droits='lecteur';" % (id)
    cur = CO.cursor()

    try:
        cur.execute(sql_feeds_lecteur)
        row = cur.fetchone()
        if row is None:
            print('Aucun flux de lecture')
            return

        print('\n=== Lectures de {0} ==='.format(row[0]))
        while row:
            print('{0}'.format(row[2]))
            row = cur.fetchone()

    except psycopg2.Error as e:
        print(e)

    input('\nContinuer >')
    return

def display_menu():
    answer = ''
    while answer != 'X':
        print('\n=== Menu ===\n')
        print('(1) Afficher mon profil')
        print('(2) Rechercher un utilisateur')
        print('(3) Consulter mes flux')
        print('(4) Créer un groupe')
        print('(X) Quitter')

        answer = input('> ')
        if answer == '1':
            display_profil(USER['id'])
        if answer == '2':
            search_user()
        if answer == '3':
            display_feeds(USER['id'])
        if answer == '4':
            create_group()
    return

def main():
    print('Connecting....')

    try:
        CO.cursor()
    except psycopg2.Error as e:
        print('Échec de la connexion à la base de données')
        return

    USER = welcome()

    allowed = identify(USER['email'], USER['password'])

    if not allowed:
        print('Les identifiants sont éronnés')
    else:
        display_menu()

    print('Disconnecting...')
    CO.close()

main()
