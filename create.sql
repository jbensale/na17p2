DROP VIEW v_profil;
DROP VIEW v_author;
DROP VIEW v_dashboard_flux;
DROP VIEW v_publication_comments;
DROP VIEW v_publication_reponse;
DROP VIEW v_publication;
DROP VIEW v_experiences_ut;
DROP VIEW v_entreprise;
DROP VIEW v_groupes_ut;

DROP TABLE groupe_utilisateurs;
DROP TABLE commentaire;
DROP TABLE lecteur_flux;
DROP TABLE redacteur_flux;
DROP TABLE publication;
DROP TABLE flux;
DROP TABLE groupe;
DROP TABLE utilisateur;
DROP TABLE entreprise;
DROP TABLE theme;

CREATE TABLE entreprise (
  id SERIAL PRIMARY KEY,
  nom VARCHAR(100) NOT NULL,
  description TEXT,
  nationalite VARCHAR(100),
  president VARCHAR(100),
  siege JSON,
  taille INTEGER
);

CREATE TABLE theme (
  nom VARCHAR PRIMARY KEY,
  description TEXT NOT NULL
);

CREATE TABLE utilisateur (
  id SERIAL PRIMARY KEY,
  email VARCHAR(100) UNIQUE NOT NULL,
  motdepasse VARCHAR(100) NOT NULL,
  nom VARCHAR(100) NOT NULL,
  prenom VARCHAR(100) NOT NULL,
  anniversaire DATE,
  entreprise INTEGER NOT NULL REFERENCES entreprise(id),
  genre CHAR(1),
  pays VARCHAR(100),
  experiences JSON NOT NULL,
  CHECK (genre IN ('f', 'm', 'o')),
  CHECK (LENGTH(motdepasse) >= 12)
);

CREATE TABLE groupe (
  id SERIAL PRIMARY KEY,
  nom VARCHAR(150) UNIQUE,
  confidentialite CHAR(4) NOT NULL,
  responsable INTEGER NOT NULL REFERENCES utilisateur(id),
  CHECK (confidentialite IN ('priv', 'publ', 'prot'))
);

CREATE TABLE groupe_utilisateurs (
  id_ut INTEGER REFERENCES utilisateur(id),
  id_gr INTEGER REFERENCES groupe(id),
  PRIMARY KEY (id_ut, id_gr)
);

CREATE TABLE flux (
  id SERIAL PRIMARY KEY,
  titre VARCHAR(150) UNIQUE NOT NULL,
  description TEXT,
  theme VARCHAR NOT NULL REFERENCES theme(nom),
  responsable INTEGER REFERENCES utilisateur(id),
  activite VARCHAR DEFAULT 'actif',
  CHECK (activite IN ('actif', 'inactif', 'archive'))
);

CREATE TABLE redacteur_flux (
  id_gr INTEGER REFERENCES groupe(id),
  id_fl INTEGER REFERENCES flux(id),
  PRIMARY KEY (id_gr, id_fl)
);

CREATE TABLE lecteur_flux (
  id_gr INTEGER REFERENCES groupe(id),
  id_fl INTEGER REFERENCES flux(id),
  PRIMARY KEY (id_gr, id_fl)
);

CREATE TABLE publication (
  id SERIAL NOT NULL,
  titre VARCHAR(100) UNIQUE NOT NULL,
  auteur INTEGER NOT NULL REFERENCES utilisateur(id),
  contenu TEXT NOT NULL,
  creation DATE DEFAULT now(),
  etat VARCHAR NOT NULL,
  dislikes INTEGER DEFAULT 0,
  likes INTEGER DEFAULT 0,
  reference INTEGER,
  flux INTEGER NOT NULL REFERENCES flux(id),
  PRIMARY KEY (id, flux),
  FOREIGN KEY (reference, flux) REFERENCES publication(id, flux),
  CHECK (etat IN ('validé', 'brouillon'))
);

CREATE TABLE commentaire (
  id SERIAL NOT NULL,
  flux INTEGER NOT NULL,
  texte TEXT NOT NULL,
  auteur INTEGER NOT NULL REFERENCES utilisateur(id),
  creation DATE DEFAULT now(),
  likes INTEGER DEFAULT 0,
  dislikes INTEGER DEFAULT 0,
  publication INTEGER NOT NULL,
  PRIMARY KEY (id, publication),
  FOREIGN KEY (id, publication) REFERENCES commentaire(id, publication),
  FOREIGN KEY (publication, flux) REFERENCES publication(id, flux)
);
