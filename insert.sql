INSERT INTO entreprise(nom, nationalite, siege, description) VALUES('Cyanide', 'française', '{"pays": "France", "ville": "Nanterre", "adresse": "3 BD DES BOUVETS", "cp": 92000}', 'Cyanide est un studio indépendant de développement de jeux vidéo basé à Nanterre, près de Paris, et Créé en 2000 par 7 anciens employés d’Ubisoft, il est devenu l’un des studios les plus productifs en France.');
INSERT INTO entreprise(nom, nationalite, siege, description) VALUES('Ubisoft', 'française', '{"pays": "France", "ville": "Rennes", "adresse": "107 AV HENRI FREVILLE", "cp": 35200}', 'Ubisoft est un créateur de mondes, qui a pour ambition d''enrichir la vie des joueurs en créant des expériences de jeu originales et mémorables.');
INSERT INTO entreprise(nom, nationalite, siege, description) VALUES('Quantic Dream', 'française', '{"pays": "France", "ville": "Paris", "adresse": "56 BD DAVOUT", "cp": 75020}', 'Quantic Dream est un développeur français de jeu vidéo basé à Paris, fondé par David CAGE en 1997. Le studio a créé depuis des jeux AAA basés sur la narration et l’émotion');
INSERT INTO entreprise(nom) VALUES('Google');
INSERT INTO entreprise(nom) VALUES('Youtube');

INSERT INTO theme VALUES('Management', 'Méthodes de management en entreprise');
INSERT INTO theme VALUES('Télétravail', 'Mise en oeuvre du télétravail et retours d''expérience');
INSERT INTO theme VALUES('Sensibilisation sécurite', 'Formations et mise en pratique de la sécurité en entreprise');
INSERT INTO theme VALUES('Divers', 'Flux divers');
INSERT INTO theme VALUES('SI', 'Services informatiques');
INSERT INTO theme VALUES('Actualités', 'Sujets d''Actualités');

INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('lhauchard@lilo.org', 'lucashauchard', 'Hauchard', 'Lucas', '[{"metier": "vidéaste", "entreprise": "Youtube", "duree": "3 ans"}]', 5);
INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('alain.corre@lilo.org', 'alaincorre_12345', 'Corre', 'Alain', '[{"metier": "DG", "entreprise": "Ubisoft", "duree": "8 ans"}]', 2);
INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('yves-guillemot@lilo.org', 'yvesguillemot', 'Guillemot', 'Yves', '[{"metier": "PDG", "entreprise": "Ubisoft", "duree": "8 ans"}]', 2);
INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('benadiba-ayoub@lilo.org', 'ayoubbenadiba', 'Benadiba', 'Ayoub', '[{"metier": "Développeur web", "entreprise": "Quantic Dream", "duree": "5 ans"}]', 1);
INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('amaurychartre@lilo.org', 'amaurychartre', 'Chartré', 'Amaury', '[{"metier": "Développeur", "entreprise": "Microsoft", "duree": "3 ans"}, {"metier": "Développeur", "entreprise": "Google", "duree": "1 ans"}]', 3);
INSERT INTO utilisateur(email, motdepasse, nom, prenom, experiences, entreprise) VALUES('theodore-laville@lilo.org', 'theodorelaville', 'Laville', 'Théodore', '[{"metier": "Manager", "entreprise": "Google", "duree": "1 ans"}]', 4);

INSERT INTO groupe(nom, confidentialite, responsable) VALUES('Games Studios', 'prot', 1);
INSERT INTO groupe(nom, confidentialite, responsable) VALUES('Tech', 'priv', 2);
INSERT INTO groupe(nom, confidentialite, responsable) VALUES('Programmers', 'prot', 1);
INSERT INTO groupe(nom, confidentialite, responsable) VALUES('Linux', 'publ', 5);

-- user -> groups
-- 1 -> 1/3
-- 2 -> 1/2
-- 3 -> 1/2/3
-- 4 -> 4
-- 5 -> 4
-- 6 -> 4
INSERT INTO groupe_utilisateurs VALUES(1, 1);
INSERT INTO groupe_utilisateurs VALUES(1, 3);
INSERT INTO groupe_utilisateurs VALUES(2, 1);
INSERT INTO groupe_utilisateurs VALUES(2, 2);
INSERT INTO groupe_utilisateurs VALUES(3, 1);
INSERT INTO groupe_utilisateurs VALUES(3, 3);
INSERT INTO groupe_utilisateurs VALUES(3, 2);
INSERT INTO groupe_utilisateurs VALUES(4, 4);
INSERT INTO groupe_utilisateurs VALUES(5, 4);
INSERT INTO groupe_utilisateurs VALUES(6, 4);

-- responsible -> feed
-- 1 -> 2
-- 2 -> 1/4
-- 3 -> 3
INSERT INTO flux(titre, theme, responsable) VALUES('Covid mises à jour', 'Actualités', 2);
INSERT INTO flux(titre, theme, responsable) VALUES('ac2020', 'Divers', 1);
INSERT INTO flux(titre, theme, responsable) VALUES('Reprise d''activité', 'Actualités', 3);
INSERT INTO flux(titre, theme, responsable) VALUES('Maintien du télétravail', 'Télétravail', 2);

-- author -> publication(feed)
-- 1 -> 1(1) 3(2) 5(2)
-- 2 -> 2(1) 6(4)
-- 3 -> 4(3) 7(3)
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(1, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', Now(), 'brouillon', 1);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(2, 'Sed ut', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt', now(), 'validé', 1);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(1, 'Neque porro', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?', now(), 'brouillon', 2);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(3, 'Quis', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', now(), 'validé', 3);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(1, 'At vero', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.', now(), 'validé', 2);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(2, 'Nam libero', 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum', now(), 'validé', 4);
INSERT INTO publication(auteur, titre, contenu, creation, etat, flux) VALUES(3, 'Ut enim ad', 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', now(), 'validé', 3);

-- group -> feed
-- 1 -> 2
-- 2 -> 1/4
-- 3 -> 3/4
INSERT INTO redacteur_flux VALUES(1, 2);
INSERT INTO redacteur_flux VALUES(2, 1);
INSERT INTO redacteur_flux VALUES(2, 4);
INSERT INTO redacteur_flux VALUES(3, 3);
INSERT INTO redacteur_flux VALUES(3, 4);

-- group -> feed
-- 1 -> 1/3
-- 2 -> 2/3
-- 3 -> 3
-- 4 -> 3
INSERT INTO lecteur_flux VALUES(1, 1);
INSERT INTO lecteur_flux VALUES(1, 3);
INSERT INTO lecteur_flux VALUES(2, 2);
INSERT INTO lecteur_flux VALUES(2, 3);
INSERT INTO lecteur_flux VALUES(3, 3);
INSERT INTO lecteur_flux VALUES(4, 3);

-- author -> comment(publication(feed))
-- 1 -> 2(1(1)) 4(3(2))
-- 2 -> 1(1(1)) 3(2(1))
INSERT INTO commentaire(flux, publication, auteur, texte) VALUES(1, 1, 2, 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.');
INSERT INTO commentaire(flux, publication, auteur, texte, likes, dislikes) VALUES(1, 1, 1, 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 2);
INSERT INTO commentaire(flux, publication, auteur, texte, likes) VALUES(1, 2, 2, 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 3);
INSERT INTO commentaire(flux, publication, auteur, texte) VALUES(2, 3, 1, 'Et harum quidem rerum facilis est et expedita distinctio');
