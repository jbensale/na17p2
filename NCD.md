# Note de Clarification (sujet 7)

## Description du projet

L'objectif de ce projet est de réaliser une application collaborative de partage de contenu. L'architecture de type 3 tiers comprend la base de données, le backend et (optionnellement) le frontend.

## Liste des objets nécessaires à la modélisation

**Utilisateur**
- possède: Email (unique), Nom, Prénom, Age, Date de naissance, Genre, Pays, Expériences
- appartient à une entreprise dont il peut consulter les autres membres
- possède une page "home/profile" avec: Nom, Prénom, Email, Groupes, Flux (dashboard)
- peut créer un flux (automatiquement responsable)
- peut créer un groupe (automatiquement responsable)
- peut voter pour une publication dont il a au moins les droits de lecture
- peut être responsable de flux et en valider/dévalider/rejeter/modifier/supprimer les publications
- peut être rédacteur de flux (appartenir au groupe rédacteur) et en modifier/supprimer les publications
- peut commenter les publications de ses flux
- peut demander à intégrer un groupe

**Flux De Publication**
- possède: Titre
- possède des utilisateurs lecteurs et rédacteurs
- possède une page d'administration (ajout de publication)
- possède un responsable utilisateur (muable)
- relatif à un thème (liste définie par l'administrateur)
- possède un statut d'activité: actif (dernière publication <= 1 mois), inactif (dernière publication > 1 mois), archive (dernière publication > 12 mois)

**Publication**
- possède: Titre, Lien, Date de publication, Créateur
- possède un score en nombre de "Like"
- possède un état: "brouillon" (modifiable et visible seulement par le responsable) ou "validé" (visible par les autres et non modifiable)
- peut repasser de validé à brouillon (et inversement)
- possède des commentaires associés

**Commentaire**
- possède: Date, Contenu, Auteur
- possède un nombre de likes et un nombre de dislikes

**Groupe d'Utilisateurs**
- possède: Nom
- peut avoir les droits d'écriture et de lecture (rédacteur) ou de lecture (lecteur) sur un flux de publication
- peut être un groupe public (visible par les utilisateurs non-membres qui peuvent faire une demande d'intégration, les membres sont visibles également), protégé (groupe visible mais pas ses membres) ou privé (non visible des non-membres qui ne peuvent donc pas demander à l'intégrer)
- possède un responsable utilisateur (muable)

**Entreprise**
- possède: Nom, Siège, Nationalité, Description, Président

## Rôles

**Lecteur/Rédacteurs:**
Les flux de publication sont liés à des groupes de **rédacteurs**, qui peuvent lire et écrire (ont accès à la page d'administration), et des groupes de **lecteurs**, qui peuvent uniquement lire les publications.

**Responsables:**
Un utilisateur peut être **responsable d'un flux** de publications quand il le créé: dans ce cas, il a le droit de transférer sa responsabilité et de définir les lecteurs/rédacteurs.

Un utilisateur peut être **responsable d'un groupe** quand il le créé: dans ce cas, il a le droit de transférer sa responsabilité et d'ajouter/supprimer des utilisateurs du groupe.

## Contraintes

    C0: un flux possède au moins un groupe rédacteur et un groupe lecteur
    C1: un utilisateur vote au plus une fois pour une publication
    C2: le responsable d'un flux est unique (et doit être rédacteur)
    C3: le responsable d'un groupe est unique
    C4: un utilisateur est collaborateur d'une seule entreprise

## Hypothèses faites pour la modélisation

    H0: le vote d'un utilisateur peut être modifié
    H1: un utilisateur peut voter s'il est rédacteur ou lecteur d'une publication
    H2: le score minimum de like d'une publication est forcément négatif
    H3: un utilisateur peut altérer les publications qu'il a lui même écrit dans les flux dont il est rédacteur, alors que le responsable du flux peut altérer toutes les publications du flux.
    H4: les commentaires sous une publication peuvent être supprimés ou modifiés par leur auteur
    H4.1: les commentaires sous une publication peuvent être supprimés par les rédacteurs
    H4.2: les commentaires sous une publication peuvent être "liké" ou "disliké" comme les publications
    H5: les rédacteurs comme les lecteurs peuvent commenter les publications de leurs flux
    H6: pouvoir "modifier" regroupe au sens large les actions de modification, suppression, refus...
    H7: une publication est liée à une seule autre publication (publication "réponse")
    H8: l'email d'un utilisateur doit être unique et est utilisé pour l'identifier
